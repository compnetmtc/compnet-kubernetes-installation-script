# Join with Master Node with AWS provider
cat << EOF | sudo tee /etc/kubernetes/node.yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: "u8xgn5.hb6qv6kpeuc8wb7i"
    apiServerEndpoint: "172.31.17.89:6443"
    caCertHashes:
      - "sha256:16520bbd941ec81bbb68f978b8f6aefae1d4ae86a32c186feb663ec1fcd1e3bb"
nodeRegistration:
  name: ip-172-31-28-109.ap-southeast-1.compute.internal
  kubeletExtraArgs:
    cloud-provider: aws
EOF

sudo kubeadm join --config /etc/kubernetes/node.yaml
