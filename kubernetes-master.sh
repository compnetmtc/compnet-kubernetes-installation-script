# Setup Network with AWS provider
cat << EOF | sudo tee /etc/kubernetes/aws.yaml  
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  serviceSubnet: "10.0.0.0/16"
  podSubnet: "10.100.0.0/24"
apiServer:
  extraArgs:
    cloud-provider: "aws"
controllerManager:
  extraArgs:
    cloud-provider: "aws"
EOF

sudo kubeadm init --config /etc/kubernetes/aws.yaml --ignore-preflight-errors=all



mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

kubeadm token create --print-join-command
